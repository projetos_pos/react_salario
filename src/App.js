import React, { Component } from 'react';

import { interval } from 'rxjs';
import { takeUntil, map, filter } from 'rxjs/operators';

import { Functions } from './utils/Functions';

import { SalarioPure } from './components/SalarioPure';
import { SalarioReversoPure } from './components/SalarioReversoPure';

class App extends Component {

  constructor() {
    super();

    this.state = {
      salario_desejado: 0,
      salario_liquido: 0,
      salario_bruto: 0,
      base_inss: 0,
      desconto_inss: 0,
      base_irpf: 0,
      desconto_irpf: 0,
    };

    //this.calcPayment = this.calcPayment.bind(this);
  }

  calcPayment(event) {
    const funcs = new Functions();

    const salario_bruto = +event.target.value;
    const base_inss = salario_bruto;
    const desconto_inss = funcs.getDiscountINSS(base_inss);
    const base_irpf = salario_bruto - desconto_inss;
    const desconto_irpf = funcs.getDiscountIRPF(base_irpf);
    const salario_liquido = base_irpf - desconto_irpf;

    this.setState({
      salario_bruto: salario_bruto === 0 ? '' : salario_bruto,
      salario_liquido: salario_liquido.toFixed(2),
      base_inss: base_inss.toFixed(2),
      desconto_inss: desconto_inss.toFixed(2),
      base_irpf: base_irpf.toFixed(2),
      desconto_irpf: desconto_irpf.toFixed(2)
    });

  }

  calcReversePayment(event) {

    /**
     * Caso o usuário digite 'Enter', ele dispara o evento.
     */
    if (event.keyCode !== 13) {
      return;
    }

    let salario_desejado = +event.target.value;
    this.calcGeneralReversePayment(salario_desejado);
  }

  calcReversePayment2() {
    let salario_desejado = +this.state.salario_desejado;
    this.calcGeneralReversePayment(salario_desejado);
  }

  calcGeneralReversePayment(salario_desejado) {
    /**
     * Evita começar do zero
     */
    let salario_bruto = salario_desejado;
    this.setState({ salario_desejado: salario_desejado });

    const obs$ = interval(1).pipe(
      /**
       * Transformação de dados (map)
       */
      map(() => {
        /**
         * Obtendo o salário líquido do momento
         */
        const currentValue = +this.state.salario_liquido;

        /**
         * Calculando a diferença entre o salário líquido do momento
         * e o salário líquido desejado
         */
        const difference = Math.abs(currentValue - +salario_desejado);

        /**
         * Quando a diferença for menor que 5 reais, o
         * incremento passa a ser de 1 centavo (0.01)
         * para uma melhor precisão no cálculo sem que
         * o mesmo se torne lento, ou seja, enquanto a
         * diferença for maior que 5 reais o incremento
         * é de "1 em 1 real"
         */
        const increment = difference >= 5 ? 1 : 0.01;

        /**
         * Incrementando o valor no salário bruto
         * e formatando para 2 casas decimais
         */

        salario_bruto = (+salario_bruto + increment).toFixed(2);

        /**
         * Atualizando o salário bruto. Quando atualizamos o valor
         * "na mão", o Vue não consegue monitorar as
         * mudanças
         */
        this._updateValores(salario_bruto);

        /**
         * Por fim, retornamos o salário líquido atual
         */

        return this.state.salario_liquido;
      }),
    );

    const match$ = obs$.pipe(
      filter(currentValue => +currentValue >= +this.state.salario_desejado),
    );

    obs$.pipe(takeUntil(match$)).subscribe();
  }

  _updateValores(salario_bruto) {
    const funcs = new Functions();

    const base_inss = salario_bruto;
    const desconto_inss = funcs.getDiscountINSS(base_inss);
    const base_irpf = salario_bruto - desconto_inss;
    const desconto_irpf = funcs.getDiscountIRPF(base_irpf);
    const salario_liquido = base_irpf - desconto_irpf;

    this.setState({
      salario_bruto: salario_bruto,
      salario_liquido: salario_liquido.toFixed(2),
      base_inss: base_inss,
      desconto_inss: desconto_inss.toFixed(2),
      base_irpf: base_irpf.toFixed(2),
      desconto_irpf: desconto_irpf.toFixed(2)
    });
  }

  setWishValue(event) {
    this.setState({ salario_desejado: +event.target.value });
  }

  render() {
    return (
      <div className="container">
        <h4 className="center-align">React Salário</h4>
        <div className="row">
          <div className="col s6">
            {/* Usando bind apenas para estudar */}
            <SalarioPure
              onChangePayment={(event) => this.calcPayment(event)}
              salario={this.state} />
          </div>

          <div className="col s6">
            <SalarioReversoPure
              onChangeReversePayment={(event) => this.calcReversePayment(event)}
              onChangeWish={(event) => this.setWishValue(event)}
              onClickPayment={() => this.calcReversePayment2()}
            />
          </div>

        </div>

      </div>
    );
  }
}

export default App;
