
export class Functions {


    constructor() {
        this.state = {
            TETO_INSS_2018: 621.04,
            tabelaINSS_2018: [
                {
                    ate: 1693.72,
                    aliquota: 8,
                },
                {
                    ate: 2822.9,
                    aliquota: 9,
                },
                {
                    ate: 5645.8,
                    aliquota: 11,
                },
                {
                    ate: Number.MAX_SAFE_INTEGER,
                    aliquota: 11,
                },
            ],
            tabelaIRPF_2018: [
                {
                    ate: 1903.98,
                    aliquota: 0,
                    deducao: 0,
                },
                {
                    ate: 2826.65,
                    aliquota: 7.5,
                    deducao: 142.8,
                },
                {
                    ate: 3751.05,
                    aliquota: 15.0,
                    deducao: 354.8,
                },
                {
                    ate: 4664.68,
                    aliquota: 22.5,
                    deducao: 636.13,
                },
                {
                    ate: Number.MAX_SAFE_INTEGER,
                    aliquota: 27.5,
                    deducao: 869.36,
                },
            ]
        };
    }

    _calcularDescontoINSS(base_inss) {
        let descontoINSS = 0;

        /**
         * Pra cada item da tabelaINSS, procuramos
         * onde a baseINSS "se encaixa". Assim que
         * acharmos, calculamos o desconto e
         * "quebramos" o loop para evitar cálculos
         * desnecessários
         */

        for (let item of this.state.tabelaINSS_2018) {
            if (base_inss <= item.ate) {
                /**
                 * Nesse momento, encontramos o item correto.
                 * Precisamos calcular o desconto com base no teto do INSS.
                 * Por isso, o Math.min auxilia garantindo o valor
                 * mínimo com base em TETO_INSS_2018
                 */
                descontoINSS = Math.min(base_inss * (item.aliquota / 100), this.state.TETO_INSS_2018);

                /**
                 * Uma vez que encontramos, forçamos o
                 * encerramento do for para evitar
                 * calculos desnecessários (performance)
                 */
                break;
            }
        }

        return descontoINSS;
    }

    /**
     * O cálculo é bastante semelhante ao do INSS,
     * exceto pelo teto, que não existe e pela aplicação
     * da dedução
     */
    _calcularDescontoIRPF(base_irpf) {
        let descontoIRPF = 0;

        for (let item of this.state.tabelaIRPF_2018) {
            if (base_irpf <= item.ate) {
                descontoIRPF = base_irpf * (item.aliquota / 100);
                //Aplicando a dedução
                descontoIRPF -= item.deducao;
                break;
            }
        }

        return descontoIRPF;
    }

    getDiscountINSS(base_inss){
        return this._calcularDescontoINSS(base_inss);
    }

    getDiscountIRPF(base_irpf){
        return this._calcularDescontoIRPF(base_irpf);
    }
}