import React, { Component } from 'react';

export class SalarioReversoPure extends Component {

    render() {
        return (
            <div className="row">
                <div className="col s12">
                    <h5 className="center-align">Cálculo reverso com Observables</h5>

                    <div className="row">
                        <div className="input-field col s6">
                            <i className="material-icons prefix">attach_money</i>
                            <input type="number" min="0" id="salario-desejado"
                             onKeyUp={this.props.onChangeReversePayment}
                             onChange={this.props.onChangeWish}  />
                            <label htmlFor="salario-desejado">Salário líquido desejado:</label>
                        </div>

                        <div className="input-field col s6">
                            <button onClick={this.props.onClickPayment} className="waves-effect waves-light btn">Calcular</button>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

