import React, { Component } from 'react';

export class SalarioPure extends Component {

    render() {
        return (
            <div className="row">
                <div className="col s12">
                    <h5 className="center-align">Cálculo em tempo real</h5>

                    <div className="row">
                        <div className="input-field col s6">
                            <i className="material-icons prefix">attach_money</i>
                            <input autoFocus type="number" id="salario-bruto" min="0"
                                value={this.props.salario.salario_bruto} 
                                onChange={this.props.onChangePayment} />
                            <label htmlFor="salario-bruto">Salário Bruto:</label>
                        </div>

                        <div className="input-field col s6">
                            <i className="material-icons prefix">attach_money</i>
                            <input disabled={true} type="number" id="salario-liquido"
                                value={this.props.salario.salario_liquido} 
                                />
                            <label htmlFor="salario-liquido">Salário Líquido:</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s6">
                            <i className="material-icons prefix">attach_money</i>
                            <input disabled={true} type="number" id="base-inss"
                                value={this.props.salario.base_inss} />
                            <label htmlFor="base-inss">Base INSS:</label>
                        </div>

                        <div className="input-field col s6">
                            <i className="material-icons prefix">attach_money</i>
                            <input disabled={true} type="number" id="desconto-inss"
                                value={this.props.salario.desconto_inss} />
                            <label htmlFor="desconto-inss">Desconto INSS:</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="input-field col s6">
                            <i className="material-icons prefix">attach_money</i>
                            <input disabled={true} type="number" id="base-irpf"
                                value={this.props.salario.base_irpf} />
                            <label htmlFor="base-irpf">Base IRPF:</label>
                        </div>

                        <div className="input-field col s6">
                            <i className="material-icons prefix">attach_money</i>
                            <input disabled={true} type="number" id="desconto-irpf"
                                value={this.props.salario.desconto_irpf} />
                            <label htmlFor="desconto-irpf">Desconto IRPF:</label>
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

